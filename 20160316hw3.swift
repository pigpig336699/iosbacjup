import Foundation

var YYYY = 2015
var MM = 03
var DD = 16


//檢查日期輸入問題
func isCorrectDate(y:Int, m:Int, d: Int) -> Bool {
    var d_:[Int] = [31,28,31,30,31,30,31,31,30,31,30,31]
    if ((y%4==0)&&(y%100 != 0)||(y%400==0)){ d_[1] = 29}
    if m<1 || m > 12 {
        return false
    }
    if d<1 || d > d_[m-1] {
        return false
    }
    return true
}

if isCorrectDate(YYYY, m: MM, d: DD){


//  第一種方法 系統呼叫

 //宣告
    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    var dateComponents = NSDateComponents()
    
    //設定日期格式
    dateFormatter.locale = NSLocale(localeIdentifier: "zh_TW")
    dateFormatter.dateFormat = "EEEE"
    //指定日期
    dateComponents.year = YYYY
    dateComponents.month = MM
    dateComponents.day = DD
    let d = NSDate()
    var date = calendar.dateFromComponents(dateComponents)
    print(dateFormatter.stringFromDate(date!), terminator:"")

//  第二種方法 菜樂攻式實作

var year = YYYY;
var month = MM;
var day = DD;
var week = 0;
 var century = year / 100;
year = year % 100;
 week = year + year / 4 + century / 4 - 2 * century + (26 * (month + 1))/10 + day - 1;
 week = week % 7;


switch week {
    case 1:
        print("星期一", terminator:"")
    case 2:
        print("星期二", terminator:"") 
    case 3:
        print("星期三", terminator:"")    
    case 4:
        print("星期四", terminator:"")    
    case 5:
        print("星期五", terminator:"")
    case 6:
        print("星期六", terminator:"")
    case 0:
        print("星期日", terminator:"")
    default :
        print("")
        
        
}
    
}else{
    print("error", terminator:"")
}



